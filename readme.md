submitter
===
ラップトップで作業した課題をワークステーションに送るツール

## Install
First
```
git clone http://github.com/greyia/submitter
cd ./bin/
sudo ln -s `pwd`/submitter /usr/local/bin/submitter 
```
Last, edit ~/.ssh/config



## Example

### alg1
```
submitter -H u-aizu -T alg1 push --ex 8 -f prog2.c 
```

```
submitter -H u-aizu -T alg1 push --ex 6 -f prog*.c 
```
```
submitter -H u-aizu -T alg1 list --ex 6 
```



### Java1

```
submitter -H u-aizu -T java1 push --ex 6 --P2 Task.pdf
```

```
submitter -H u-aizu -T java1 push --ex 3 --P1 Task.pdf --P2 Test.java --P3 Test2.java
```

```
submitter -H u-aizu -T java1 list --ex 2
```
